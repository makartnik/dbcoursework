﻿using System.Collections.Generic;

namespace DBCoursework.Models
{
    public class Competition
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public virtual ICollection<Team> Participants { get; set; }
    }
}