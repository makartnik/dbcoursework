﻿using System.ComponentModel.DataAnnotations;

namespace DBCoursework.Models
{
    public class Player
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Number { get; set; }

        public int? TeamId { get; set; }

        public virtual Team Team { get; set; }
    }
}