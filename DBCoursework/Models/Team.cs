﻿using System.Collections.Generic;

namespace DBCoursework.Models
{
    public class Team
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public virtual ICollection<Player> Players { get; set; }

        public virtual ICollection<Competition> Competitons { get; set; }

    }
}