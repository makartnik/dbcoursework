﻿using System.Data.Entity;

namespace DBCoursework.Models
{
    public class FootballContext : DbContext
    {
        public System.Data.Entity.DbSet<DBCoursework.Models.Player> Players { get; set; }

        public System.Data.Entity.DbSet<DBCoursework.Models.Team> Teams { get; set; }

        public System.Data.Entity.DbSet<DBCoursework.Models.Competition> Competitions { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Team>();
            modelBuilder.Entity<Player>().HasRequired(p => p.Team).WithMany(t => t.Players)
                .HasForeignKey(p => p.TeamId);
            modelBuilder.Entity<Competition>().HasMany(c => c.Participants).WithMany(t => t.Competitons);
        }
    }
}